﻿using System;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    private IMovable _currentMovement;
    private bool _isOnMove;
    
    private void Update()
    {
        if (_isOnMove)
        {
            _currentMovement.DoMove();
        }
    }

    public void SetMovement(IMovable movement)
    {
        _currentMovement = movement;
    }
    
    public void StartMovement()
    {
        _isOnMove = true;
    }
    
    public void StopMovement()
    {
        _isOnMove = false;
    }
}