using UnityEngine;

public class EnemyMover : IMovable
{
    private Transform _ownTransform;
    private Transform _playerTransform;
    private float _velocity;

    public EnemyMover(Transform ownTransform, Transform playerTransform, float velocity)
    {
        _ownTransform = ownTransform;
        _playerTransform = playerTransform;
        _velocity = velocity;
    }

    public void DoMove()
    {
        _ownTransform.position = Vector3.MoveTowards(_ownTransform.position,_playerTransform.position,_velocity*Time.deltaTime);
    }

    public void SetValue(object[] values)
    {
        
    }
}