﻿using System;
using System.Collections.Generic;

public class GunAttributesStorekeeper
{
    private Dictionary<GunAttributeType, Action> _gunAttributesFunctions = new();


    public void AddFunction(GunAttributeType gunAttributeType, Action function)
    {
        _gunAttributesFunctions.Add(gunAttributeType,function);
    }

    public void InvokeFunction(object attribute)
    {
        GunAttributeType gunAttributeType = (GunAttributeType)attribute;
        _gunAttributesFunctions[gunAttributeType]?.Invoke();
    }

}