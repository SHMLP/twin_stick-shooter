﻿using System;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    
    [Header("Spawn")]
    [SerializeField] private float _spawnInternalRadius;
    [SerializeField] private float _spawnExternalRadius;

    [Header("Movement")] 
    [SerializeField] private float _velocity;

    [Header("Waves")] 
    [SerializeField] private int _enemiesIncreasePerWave;
    [SerializeField] private int _waveDuraction;
    

    private Enemy[] _enemiesContainer;
    [SerializeField]private Enemy[] _enemiesToSpawn;
    
    private Transform _playerTransform;
    
    private RoundStatesFunctionStorekeeper _roundStatesFunctionStorekeeper;
    private EnemyArrayAdministrator _enemyArrayAdministrator;
    
    private int _lastRemoveIndex;
    private int _lastAddIndex;
    private int _maxNumberOfEnemies;

    private Action _sendMoney;
    private void Awake()
    {
        AddComponents();
        _enemiesContainer = GetComponentsInChildren<Enemy>(true);
        _enemiesToSpawn = new Enemy[_enemiesContainer.Length];
        Array.Copy(_enemiesContainer,_enemiesToSpawn,_enemiesContainer.Length);
    }

    private void AddComponents()
    {
        _enemyArrayAdministrator = new EnemyArrayAdministrator();
        _enemyArrayAdministrator.SubscribeStartEnemyAction(StartEnemy);
    }
    
    public void SetNumberOfEnemies(int maxNumberOfEnemies)
    {
        _maxNumberOfEnemies = Mathf.Clamp(maxNumberOfEnemies,1,_enemiesContainer.Length);
        Debug.Log(_maxNumberOfEnemies);
    }
    private void InitEnemies()
    {
        foreach (Enemy item in _enemiesContainer)
        {
            item.InitComponents(_playerTransform,_spawnInternalRadius,_spawnExternalRadius,_velocity);
            //item.SubscribeEnemyDeadAction(UpdateEnemiesArray);
            item._enemyDeadActionSender += UpdateEnemiesArray;
        }
    }

    public void Configure(Transform playerTransform, RoundStatesFunctionStorekeeper roundStatesFunctionStorekeeper, Action money)
    {
        _playerTransform = playerTransform;
        _roundStatesFunctionStorekeeper = roundStatesFunctionStorekeeper;
        _roundStatesFunctionStorekeeper.AddFunction(RoundStateType.EndWave,NewWave);
        _sendMoney = money;
        InitEnemies();
    }

    public void StartWaves()
    {
        //StartRound();
        AppearEnemies();
    }
    
    private void AppearEnemies()
    {
        _enemyArrayAdministrator.RemoveEnemiesFromArray(_enemiesToSpawn, ref _lastRemoveIndex, ref _maxNumberOfEnemies);
    }

    private void StartEnemy(Enemy enemy)
    {
        enemy.StartEnemy();
    }
    private void UpdateEnemiesArray(Enemy enemy)
    {
        //_enemiesToSpawn = _enemyArrayAdministrator.AddEnemyOfArray(_enemiesToSpawn, enemy, ref _lastAddIndex);
        _lastAddIndex = _enemyArrayAdministrator.UpdateIndex(_enemiesToSpawn.Length,ref _lastAddIndex);
        _sendMoney.Invoke();
        
    }
  
    private void DisappearEnemies()
    {
        for (int i = 0; i < _enemiesContainer.Length; i++)
        {
            //UpdateEnemiesArray(_enemiesContainer[i]);
            _enemiesContainer[i].Disappear();
        }
    }
    public void StopEnemies()
    {
        DisappearEnemies();
        // EndRound();
        // StopEnemiesMovement();
    }
    private void NewWave()
    {
        SetNumberOfEnemies(_maxNumberOfEnemies+_enemiesIncreasePerWave);
        AppearEnemies();
    }

    public void EndRound()
    {
       gameObject.SetActive(false);
    }

    private void StartRound()
    {
        gameObject.SetActive(true);
    }
    private void UnsubscribeAction()
    {
        foreach (Enemy item in _enemiesContainer)
        {
            item.UnsubscribeAllActions();
        }
    }
    public void Destroy()
    {
        UnsubscribeAction();
    }
}