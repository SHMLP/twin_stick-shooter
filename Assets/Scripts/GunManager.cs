﻿using UnityEngine;

public class GunManager : MonoBehaviour
{
    
    [Header("Damage")]
    [SerializeField]private int _initialDamage;
    [SerializeField]private int _addDamage;

    [Header("Speed")] 
    [SerializeField] private float _speed;
    [SerializeField]private float _addSpeed;
    
    [Header("Quantity")] 
    [SerializeField] private float _initialGunQuantity;
    [SerializeField]private float _addGun;
    
    [SerializeField]private Gun[] _gunContainer;

    private GunMovementController _gunMovementController;

    private int _lastIndex;
    
    private void Awake()
    {
        _gunContainer = GetComponentsInChildren<Gun>(true);
        
        _gunMovementController = GetComponent<GunMovementController>();
        _gunMovementController.SetMovement(new GunRotator(transform,_speed));
        
        Init();
    }

    private void Init()
    {
        foreach (Gun item in _gunContainer)
        {
            item.ConfigureInitialDamage(_initialDamage);    
        }
    }

    
    public void Begin()
    {
        ActivateInitialGuns();
        _gunMovementController.StartMovement();
    }

    public void Stop()
    {
        _gunMovementController.StopMovement();
    }
    private void ActivateInitialGuns()
    {
        for (int i = 0; i < _initialGunQuantity; i++)
        {
            _gunContainer[i].Activate();
            _lastIndex = i+1;
        }
    }

    public void AddDamage()
    {
        Debug.Log("Damage");
        foreach (Gun item in _gunContainer)
        {
            item.AddDamage(_addDamage);
        }
    }
    public void AddSpeed()
    {
        Debug.Log("Speed");
        _gunMovementController.ChangeMovementValues(new object[]{_addSpeed});
    }
    public void AddGun()
    {
        int initialIndex = _lastIndex;
        for (int i = initialIndex; i < initialIndex + _addGun; i++)
        {
            _gunContainer[i].Activate();
            _lastIndex = i+1;
        }
    }
    
}