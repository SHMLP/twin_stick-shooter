﻿using UnityEngine;

public class Controller : BController
{
    
    [SerializeField]
    private View _view;
    private void Awake()
    {
        Number = 1;
        _view = GetComponent<View>();
        AddListeners();
    }

    private void AddListeners()
    {
        _view._button.onClick.AddListener(Message);
        
    }

    public void Message()
    {
        Debug.Log("Message");
    }

    private void Num(int number)
    {
        Debug.Log($"Message {number}");
    }
}

public abstract class BController : MonoBehaviour
{
    private int number;
    private int _nextScreen;
    public int Number
    {
        get => number;
        set => number = value;
    }

    public int NextScreen
    {
        get => _nextScreen;
        set => _nextScreen = value;
    }

    public int D()
    {
        Debug.Log($"H {number}");
        return number;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}