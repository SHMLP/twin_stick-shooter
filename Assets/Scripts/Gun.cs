﻿using System;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private int _damage;
    
   
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out EnemyLife enemyLife))
        {
            enemyLife.ReduceLife(_damage);
        }
    }

    public void ConfigureInitialDamage(int initialDamage)
    {
        _damage = initialDamage;
    }
    public void AddDamage(int addDamage)
    {
        _damage += addDamage;
    }


    public void Activate()
    {
        gameObject.SetActive(true);
    }
}