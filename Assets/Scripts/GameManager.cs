﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    [Header("Round")]
    [SerializeField] private int _roundDuration;
    [SerializeField] private int _waveDuraction;
    [SerializeField] private int _storeDuraction;
    [SerializeField] private int _numberOfInitialEnemies;

    [Header("Store")]
    [SerializeField] private int _speedCost;
    [SerializeField] private int _quantityCost;
    [SerializeField] private int _damageCost;
    [SerializeField] private int _healCost;
    [SerializeField] private int _enemyCost;
    
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private Player _player;
    [SerializeField] private EnemyPool _enemyPool;
    
    private GunManager _gunManager;
    private RoundStatesFunctionStorekeeper _roundStatesFunctionStorekeeper;
    private void Start()
    {
        _gunManager = _player.GetComponentInChildren<GunManager>();
        _roundStatesFunctionStorekeeper = new RoundStatesFunctionStorekeeper();
        _enemyPool.Configure(_player.transform,_roundStatesFunctionStorekeeper,SendMoney);
        _roundStatesFunctionStorekeeper.AddFunction(RoundStateType.EndRound,EndRound);
        _roundStatesFunctionStorekeeper.AddFunction(RoundStateType.EndStore,StartGame);
        _uiManager.Init();
        _uiManager.SubscribeStartGameAction(StartGame);
        _uiManager.GetScreen(EnumScreenType.Play).Configure(new object[]{_roundDuration,_waveDuraction,_storeDuraction});
        _uiManager.GetScreen(EnumScreenType.Play).SubscribeExternalActionsSenders(_roundStatesFunctionStorekeeper.InvokeFunction);
        _uiManager.GetScreen(EnumScreenType.Store).SubscribeExternalActionsSenders(_player.InvokeMethod);
        _uiManager.GetScreen(EnumScreenType.Store).Configure(new object[]{_speedCost,_quantityCost,_damageCost,_healCost});
    }

    private void SendMoney()
    {
        Debug.Log("Money");
        _uiManager.GetScreen(EnumScreenType.Store).SubscribeMethodToExternalAction(GameResourceType.Money).Invoke(_enemyCost);
    }
    private void StartGame()
    {
        _uiManager.SetScreen(EnumScreenType.Play);
        _uiManager.GetScreen(EnumScreenType.Play).StartController();
        _player.Begin();
        _gunManager.Begin();
        _enemyPool.SetNumberOfEnemies(_numberOfInitialEnemies);
        _enemyPool.StartWaves();
    }

    private void EndRound()
    {
        _player.Stop();
        _gunManager.Stop();
        _enemyPool.StopEnemies();
        _uiManager.AppearScreen(EnumScreenType.Store);
        _uiManager.GetScreen(EnumScreenType.Play).StartController();
    }
    private void EndStore()
    {
        _uiManager.DisappearScreen(EnumScreenType.Store);
    }
    
    private void OnDestroy()
    {
        _enemyPool.Destroy();
        _player.Destroy();
    }

}