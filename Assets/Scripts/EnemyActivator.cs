﻿using System;
using UnityEngine;

public class EnemyActivator
{
    private GameObject _enemyGameObject;
    
    public EnemyActivator(GameObject enemyGameObject)
    {
        _enemyGameObject = enemyGameObject;
    }
    
    public void Activate()
    {
        _enemyGameObject.SetActive(true);
    }
    
    public void Deactivate()
    {
        _enemyGameObject.SetActive(false);
    }
    
}


