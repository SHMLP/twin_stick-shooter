﻿public interface IMovable
{
    void DoMove();
    void SetValue(object[] values);
}