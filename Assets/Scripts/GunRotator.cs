﻿using UnityEngine;

public class GunRotator : IMovable
{
    private Transform _gunTransform;
    private float _velocity;

    public GunRotator(Transform gunTransform,float velocity)
    {
        _gunTransform = gunTransform;
        _velocity = velocity;
    }

    public void SetValue(object[] values)
    {
        _velocity += GetValue<float>(values[0]);

    }
    public void DoMove()
    {
        _gunTransform.Rotate(-Vector3.forward*_velocity*Time.deltaTime);
    }
    
    protected U GetValue<U>(object parameter)
    {
        U value = (U)parameter;
        return value;
    }
}