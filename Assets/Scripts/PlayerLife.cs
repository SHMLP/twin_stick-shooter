﻿using System;
using UnityEngine;

public class PlayerLife : MonoBehaviour , ILife
{
    private int _currentLife;
    private Action _dieActionSender;
    private Action _dieActionReceiver;
    
    public void SubscribeDieAction(Action receiver)
    {
        _dieActionReceiver = receiver;
        _dieActionSender += _dieActionReceiver;
    }
    
    public void Configure(int maxlife)
    {
        _currentLife = maxlife;
    }
    
    public void ReduceLife(int amount)
    {
        _currentLife -= amount;
        if (_currentLife<=0)
        {
            _dieActionSender?.Invoke();
        }
    }
    
    public void UnsubscribeActions()
    {
        _dieActionSender -= _dieActionReceiver;
    }
}