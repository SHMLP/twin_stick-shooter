﻿using System;
using System.Collections;
using UnityEngine;

public class BControllerLocator : MonoBehaviour
{
    private BController[] _bControllersContainer;

    private void Awake()
    {
        _bControllersContainer = GetComponentsInChildren<BController>();
        StartCoroutine(CallD());
    }

    IEnumerator CallD()
    {
        yield return new WaitForSeconds(5);
        foreach (BController j in _bControllersContainer)
        {
            j.D();
        }
        
        
    }
}