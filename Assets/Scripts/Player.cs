using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _playerVelocity;
    
    private PlayerMovementController _playerMovementController;

    private GunManager _gunManager;
    private GunAttributesStorekeeper _gunAttributesStorekeeper;

    private ILife _playerLife;
    
    private event Action _playerDeadActionSender;
    private event Action _playerDeadActionReceiver;

  
    private void Awake()
    {
        _playerMovementController = GetComponent<PlayerMovementController>();
        _playerMovementController.SetMovement(new PlayerMover(transform,_playerVelocity));

        _playerLife = GetComponent<ILife>();
        _playerLife.SubscribeDieAction(OnDead);
        
        _gunManager = GetComponentInChildren<GunManager>();

        _gunAttributesStorekeeper = new GunAttributesStorekeeper();
        AddFunctionsOnStorekeeper();
    }

    private void AddFunctionsOnStorekeeper()
    {
        _gunAttributesStorekeeper.AddFunction(GunAttributeType.Speed,_gunManager.AddSpeed);
        _gunAttributesStorekeeper.AddFunction(GunAttributeType.Quantity,_gunManager.AddGun);
        _gunAttributesStorekeeper.AddFunction(GunAttributeType.Damage,_gunManager.AddDamage);
        _gunAttributesStorekeeper.AddFunction(GunAttributeType.Heal,AddLife);
    }

    public void InvokeMethod(object attribute)
    {
        _gunAttributesStorekeeper.InvokeFunction(attribute);
    }
    private void AddLife()
    {
        _playerLife.ReduceLife(-2);
    }
    public void SubscribePlayerDeadAction(Action receiver)
    {
        _playerDeadActionReceiver = receiver;
        _playerDeadActionSender += _playerDeadActionReceiver;
    }
    private void UnsubscribePlayerDeadAction()
    {
        _playerDeadActionSender -= _playerDeadActionReceiver;
    }
    private void OnDead()
    {
        _playerDeadActionSender?.Invoke();
    }
    public void Begin()
    {
        Activate();
        _playerMovementController.StartMovement();
    }

    private void Activate()
    {
        gameObject.SetActive(true);
    }
    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
    public void Stop()
    {
        Deactivate();
        _playerMovementController.StopMovement();
    }

    public void Destroy()
    {
        _playerLife.UnsubscribeActions();
    }
}