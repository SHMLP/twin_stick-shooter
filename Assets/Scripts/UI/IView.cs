using System.Collections.Generic;

public interface IView
{
    ButtonsFunctions<T>[] GetFunctionButtonsContainer<T>();
    Dictionary<T, object[]> GetParametersContainer<T>();
    Dictionary<T, object[]> GetParametersWithoutButtonsContainer<T>();
    ScreenNavigationButtons[] GetNavigationButtons();

    void Init();
}