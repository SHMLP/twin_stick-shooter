﻿using UnityEngine;
using TMPro;
public class OnGameView : AbstractBaseView<OnGameControllerFunctionsType>
{
    [SerializeField] private TMP_Text _roundTime;

    public override void Init()
    {
        base.Init();
        AddParameters();
    }
    

    private void AddParameters()
    {
        ParameterList = new object[]{_roundTime};
        ParametersWithoutButtonsContainer.Add(OnGameControllerFunctionsType.AddTime, ParameterList);
    }
    
    
}