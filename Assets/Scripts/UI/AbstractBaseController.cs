using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBaseController<T> : MonoBehaviour, IController
{
     private IView _abstractBaseView;
     private IModel _abstractBaseModel;
    
    private EnumScreenType _enumScreenType;
    
    private ScreenNavigationButtons[] _navigationButtonsContainer;
    
    private Action<EnumScreenType,EnumScreenType> _nextPageActionSender;                                      
    private Action<EnumScreenType,EnumScreenType> _nextPageActionReceiver;
    
    private ButtonsFunctions<T>[] _functionsButtonsContainer;
    private Dictionary<T, object[]> _parametersContainer = new Dictionary<T, object[]>();
    private Dictionary<T, object[]> _parametersWithoutButtonsContainer = new Dictionary<T, object[]>();
    private Dictionary<T, object[]> _modelValuesContainer = new Dictionary<T, object[]>();
    
    private T[] _functionEnumTypeContainer;
    
    private Dictionary<T,Action<object[]>> _functionsContainer = new Dictionary<T, Action<object[]>>();

    protected Dictionary<T, Action<object[]>> FunctionContainer
    {
        get => _functionsContainer;
        set => _functionsContainer = value;
    }
    
    
    protected EnumScreenType ScreenType
    {
        get => _enumScreenType;
        set => _enumScreenType = value;
    }

    protected void Init(EnumScreenType screenType, IModel model)
    {
        SetScreenType(screenType);
        GetView();
        GetModel(model);
        SetNavigationButtons();
        SetFunctionButtonsContainer();
        SetFunctionParametersContainer();
        SetFunctionValuesContainer();
        
    }
    protected void Init(EnumScreenType screenType)
    {
        SetScreenType(screenType);
        GetView();
        SetNavigationButtons();
        SetFunctionButtonsContainer();
        SetFunctionParametersContainer();
        SetParametersWithoutButtonsContainer();
    }
    
    private void SetFunctionButtonsContainer()
    {
        _functionsButtonsContainer = _abstractBaseView.GetFunctionButtonsContainer<T>();
    }
    private void SetFunctionParametersContainer()
    {
        _parametersContainer = _abstractBaseView.GetParametersContainer<T>();
    }
    private void SetParametersWithoutButtonsContainer()
    {
        _parametersWithoutButtonsContainer = _abstractBaseView.GetParametersWithoutButtonsContainer<T>();
    } 
    private void SetFunctionValuesContainer()
    {
        _modelValuesContainer = _abstractBaseModel.GetValuesContainer<T>();
    }
    private void SetScreenType(EnumScreenType screenType)
    {
        _enumScreenType = screenType;
    }
    private void GetView()
    {
        _abstractBaseView = GetComponentInChildren<IView>();
        _abstractBaseView.Init();
    }
    private void GetModel(IModel model)
    {
        _abstractBaseModel = model;
        _abstractBaseModel.Init();
    }

    public abstract Action<object> SubscribeMethodToExternalAction(Enum method);


    public abstract void SubscribeExternalActionsSenders(Action<object> receiver);
    public abstract void UnsubscribeExternalActionsSender();

    public void SubscribeActionReceiver(Action<EnumScreenType,EnumScreenType> receiver)
    {
        _nextPageActionReceiver = receiver;
        _nextPageActionSender += _nextPageActionReceiver;
    }
    
    public void UnsubscribeActionReceiver()
    {
        _nextPageActionSender -= _nextPageActionReceiver;
    }

    public virtual void StartController()
    {
        
    }

    public abstract void Configure(object[] values);

    public EnumScreenType GetUIType()                       
    {
        return _enumScreenType;
    }
    
    private void SetNavigationButtons()
    {
        _navigationButtonsContainer = _abstractBaseView.GetNavigationButtons();
    }
    private void AttachNextScreenListeners()                       
    {                                                   
        foreach (ScreenNavigationButtons item in _navigationButtonsContainer)
        {
            EnumScreenType nextScreen = item.GetEnumUIType();
            item.GetNextButton().onClick.AddListener(()=>NextScreen(nextScreen));
        }
    }
    private void RemoveNextScreenListeners()                       
    {                     
        foreach (ScreenNavigationButtons item in _navigationButtonsContainer)
        {
            item.GetNextButton().onClick.RemoveAllListeners();
        }
    } 
    private void RemoveFunctionListeners()                       
    {            
        foreach (ButtonsFunctions<T> item in _functionsButtonsContainer)
        {
            item.GetNextButton().onClick.RemoveAllListeners();
        }
       
    }

    public void NextScreen(EnumScreenType nextScreen)
    {
        _nextPageActionSender.Invoke(_enumScreenType,nextScreen);
    }
    
    public void Show()
    {
        gameObject.SetActive(true);
        AttachNextScreenListeners();
        AttachFunctionListeners();
    }

    public void Hide()
    {
        RemoveNextScreenListeners();
        RemoveFunctionListeners();
        gameObject.SetActive(false);
    }
    
    private void AttachFunctionListeners()                       
    {                          
        foreach (ButtonsFunctions<T> item in _functionsButtonsContainer)
        {
            T function = item.GetEnumFunction();
            if (_parametersContainer.TryGetValue(function,out object[] objectContainer))
            {
                objectContainer = _parametersContainer[function];
            }
            if (_modelValuesContainer.TryGetValue(function,out object[] value))
            {
               objectContainer = MergeObjectArrays(objectContainer, value);
            }
            item.GetNextButton().onClick.AddListener(()=>FunctionContainer[function](objectContainer));
        }
    }

    protected object[] GetParametersWithoutButtons(T enumType)
    {
        return _parametersWithoutButtonsContainer[enumType];
    }
    
    private object[] MergeObjectArrays(object[] parametersArray, object[] valuesArray)
    {
        object[]  objectContainer = new object[parametersArray.Length+valuesArray.Length];
        for (int i = 0; i < objectContainer.Length; i++)
        {
            if (i>=parametersArray.Length)
            {
                objectContainer[i] = valuesArray[i-parametersArray.Length];
            }
            else
            {
                objectContainer[i] = parametersArray[i];
            }
        }
        return objectContainer;
    }
    
    
    
    protected U GetParameter<U>(object parameter)
    {
        U value = (U)parameter;
        return value;
    }
}

