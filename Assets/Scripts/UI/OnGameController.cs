﻿using System;
using TMPro;
using UnityEngine;

public class OnGameController : AbstractBaseController<OnGameControllerFunctionsType>
{
    private TMP_Text _timeText;
    private float _currentTime;
    private int _currentNumber;

    private int _waveDuration;
    private int _timeDuration;
    
    private int _storeDuration;
    private int _roundDuration;
    private event Action<object> _roundActionSender;
    private event Action<object> _roundActionReceiver;
    
    private bool _isOnTime;
    private bool _isOnRound = true;
    
    private int _lastCurrentTime;
    
    
    
    

    private void Awake()
    {
        Init(EnumScreenType.Play);
        AddTimeParameters(GetParametersWithoutButtons(OnGameControllerFunctionsType.AddTime));
    }

    public override void StartController()
    {
        _isOnTime = true;
        _timeText.text = _timeDuration.ToString();
        _lastCurrentTime = 0;
        _currentTime = 0;
    }

    private void Update()
    {
        Add();
    }

    private void AddTimeParameters(object[] parameters)
    {
        _timeText = GetParameter<TMP_Text>(parameters[0]);
    }

    private void Add()
    {
        if (_isOnTime)
        {
            if (int.Parse(_timeText.text)>0)
            {
                _currentTime += Time.deltaTime;
                
                
                if (_currentTime>=_lastCurrentTime)
                {
                    _lastCurrentTime++;
                    _currentNumber = int.Parse(_timeText.text) - 1;
                    _timeText.text = _currentNumber.ToString();
                    if (_lastCurrentTime>=_waveDuration && _isOnRound)
                    {
                        _lastCurrentTime = 1;
                        _currentTime = 0;
                        _roundActionSender?.Invoke(RoundStateType.EndWave);
                    }
                }
            }
            else if(_currentNumber==0)
            {
                EndTime();
            }
        }
    }

    private void EndTime()
    {
        _isOnTime = false;
        _currentNumber = -1;
        if (_isOnRound)
        {
            _isOnRound = false;
            _timeDuration = _storeDuration;
            _roundActionSender?.Invoke(RoundStateType.EndRound);
        }
        else
        {
            _isOnRound = true;
            _timeDuration = _roundDuration;
            _roundActionSender?.Invoke(RoundStateType.EndStore);
        }
    }

    public override void Configure(object[] values)
    {
        _roundDuration = GetParameter<int>(values[0]);
        _waveDuration = GetParameter<int>(values[1]);
        _storeDuration = GetParameter<int>(values[2]);
        _timeDuration = _roundDuration;
    }

    public override Action<object> SubscribeMethodToExternalAction(Enum method)
    {
        return null;
    }

    public override void SubscribeExternalActionsSenders(Action<object> receiver)
    {
        _roundActionReceiver = receiver;
        _roundActionSender += _roundActionReceiver;
    }

    public override void UnsubscribeExternalActionsSender()
    {
        _roundActionSender -= _roundActionReceiver;
    }
    
}