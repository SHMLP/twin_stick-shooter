public enum EnumScreenType
{
    Main,
    Store,
    Play
}

public enum GunAttributeType
{
    Speed,
    Quantity,
    Damage,
    Heal
}


public enum OnGameControllerFunctionsType
{
    AddTime
}

public enum StoreControllerFunctionType
{
    AddSpeed,
    AddQuantity,
    AddDamage,
    AddHeal,
    AddMoney
}

public enum RoundStateType
{
    EndRound,
    EndStore,
    EndWave
}

public enum GameResourceType
{
    Money
}