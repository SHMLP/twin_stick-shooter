using System;
using TMPro;
using UnityEngine;

public class StoreView : AbstractBaseView<StoreControllerFunctionType>
{
    [SerializeField] private TMP_Text _moneyText;
    public override void Init()
    {
        base.Init();
        AddMoneyParameters();
    }

    private void AddMoneyParameters()
    {
        ParameterList = new object[] { _moneyText };
        ParametersWithoutButtonsContainer.Add(StoreControllerFunctionType.AddMoney,ParameterList);
    }
}