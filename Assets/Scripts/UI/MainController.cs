using System;

public class MainController : AbstractBaseController<Enum>
{
    private void Awake()
    {
        Init(EnumScreenType.Main);
    }

    public override void Configure(object[] values)
    {
        
    }

    public override Action<object> SubscribeMethodToExternalAction(Enum method)
    {
        return null;
    }

    public override void SubscribeExternalActionsSenders(Action<object> receiver)
    {
        
    }

    public override void UnsubscribeExternalActionsSender()
    {
        
    }
}