using System;

public interface IController
{
    void StartController();
    void Configure(object[] values);
    EnumScreenType GetUIType();
    Action<object> SubscribeMethodToExternalAction(Enum method);
    void SubscribeExternalActionsSenders(Action<object> receiver);
    void UnsubscribeExternalActionsSender();
    void SubscribeActionReceiver(Action<EnumScreenType, EnumScreenType> receiver);
    void UnsubscribeActionReceiver();
    void Show();
    void Hide();
}