using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ScreenNavigationButtons
{
    [SerializeField] private Button _nextButton; 
    [SerializeField] private EnumScreenType _enumScreenType;
    
    public Button GetNextButton()
    {
        return _nextButton;
    }
    public EnumScreenType GetEnumUIType()
    {
        return _enumScreenType;
    }
}