using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ButtonsFunctions<T>
{
    [SerializeField] private Button _nextButton; 
    [SerializeField] private T _enumButtonFunction;
    
    public Button GetNextButton()
    {
        return _nextButton;
    }
    public T GetEnumFunction()
    {
        return _enumButtonFunction;
    }
}