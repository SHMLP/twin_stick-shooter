using System.Collections.Generic;

public interface IModel
{
    
    Dictionary<U, object[]> GetValuesContainer<U>();
    void Init();
}