using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class UIManager : MonoBehaviour
{
    [SerializeField]private UIControllerLocator _uiControllerLocator;
    private Dictionary<EnumScreenType, IController> _uiControllersList;

    private EnumScreenType _currentScreen;
    private EnumScreenType _nextScreen;

    private event Action _startGameActionSender;
    private event Action _startGameActionReceiver;
    
    public void Init()
    {
        _uiControllerLocator.CreateControllersList();
        _uiControllersList = _uiControllerLocator.GetControllersList();
        SubscribeActionNextScreen();
        HideAllScreens();
        FirstScreen();
    }

    public void SubscribeStartGameAction(Action receiver)
    {
        _startGameActionReceiver = receiver;
        _startGameActionSender += _startGameActionReceiver;
    }
    public void UnsubscribeStartGameAction()
    {
        _startGameActionSender -= _startGameActionReceiver;
    }
    private void HideAllScreens()
    {
        foreach (IController item in _uiControllersList.Values)
        {
            item.Hide();
        }
    }

    private void FirstScreen()
    {
        _uiControllersList[EnumScreenType.Main].Show();
    }
    private void SubscribeActionNextScreen()
    {
        foreach (IController item in _uiControllersList.Values)
        {
            item.SubscribeActionReceiver(ScreenChange);
        }
    } 
    private void UnsubscribeActionNextScreen()
    {
        foreach (IController item in _uiControllersList.Values)
        {
            item.UnsubscribeActionReceiver();
        }
    }

    private void UnsubscribeScreenActions()
    {
        foreach (IController item in _uiControllersList.Values)
        {
            item.UnsubscribeExternalActionsSender();
        }
    }
    private void ScreenChange(EnumScreenType currentScreen, EnumScreenType nextScreen)
    {
        _currentScreen = currentScreen;
        _nextScreen = nextScreen;
        if (_nextScreen == EnumScreenType.Play)
        {
            _startGameActionSender?.Invoke();
            return;
        }
        _uiControllersList[currentScreen].Hide();
        _uiControllersList[nextScreen].Show();
        
    }

    public IController GetScreen(EnumScreenType screen)
    {
        return _uiControllersList[screen];
    }
    
    public void SetScreen(EnumScreenType nextScreen)
    {
        _nextScreen = nextScreen;
        _uiControllersList[_currentScreen].Hide();
        _uiControllersList[_nextScreen].Show();
        _currentScreen = nextScreen;
    }

    public void AppearScreen(EnumScreenType nextScreen)
    {
        _currentScreen = nextScreen;
        _uiControllersList[_currentScreen].Show();
    }
    public void DisappearScreen(EnumScreenType screen)
    {
        _uiControllersList[screen].Hide();
    }
    
    private void OnDestroy()
    {
        UnsubscribeActionNextScreen();
        UnsubscribeStartGameAction();
        UnsubscribeScreenActions();
    }
}
