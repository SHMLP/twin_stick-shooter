﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StoreController : AbstractBaseController<StoreControllerFunctionType>
{
    private event Action<object> _attributeActionSender;
    private event Action<object> _attributeActionReceiver;

    private Dictionary<Enum, Action<object>> _externalActionReceiver = new();

    TMP_Text _moneyText;
    private int _money;
    private int _speedCost;
    private int _quantityCost;
    private int _damageCost;
    private int _healCost;
    private void Awake()
    {
        Init(EnumScreenType.Store);
        FunctionContainer.Add(StoreControllerFunctionType.AddSpeed, AddSpeed);
        FunctionContainer.Add(StoreControllerFunctionType.AddQuantity, AddQuantity);
        FunctionContainer.Add(StoreControllerFunctionType.AddDamage, AddDamage);
        FunctionContainer.Add(StoreControllerFunctionType.AddHeal, AddHeal);
        AddMoneyParameters(GetParametersWithoutButtons(StoreControllerFunctionType.AddMoney));
        _externalActionReceiver.Add(GameResourceType.Money,AddMoney);
    }

    private void AddMoneyParameters(object[] parameters)
    {
        _moneyText = GetParameter<TMP_Text>(parameters[0]);
    }

    private void AddSpeed(object[] parameters)
    {
        if (_money >=_speedCost)
        {
            AddMoney(-_speedCost);
            _attributeActionSender?.Invoke(GunAttributeType.Speed);
        }
    }
    private void AddQuantity(object[] parameters)
    {
        if (_money >= _quantityCost)
        {
            AddMoney(-_quantityCost);
            _attributeActionSender?.Invoke(GunAttributeType.Quantity);
        }
    }
    private void AddDamage(object[] parameters)
    {
        if (_money >= _damageCost)
        {
            AddMoney(-_damageCost);
            _attributeActionSender?.Invoke(GunAttributeType.Damage);
        }
    }
    private void AddHeal(object[] parameters)
    {
        if (_money >= _healCost)
        {
            AddMoney(-_healCost);
            _attributeActionSender?.Invoke(GunAttributeType.Heal);
        }
    }

    private void AddMoney(object money)
    {
        _money = int.Parse(_moneyText.text) + GetParameter<int>(money);
        Debug.Log($"Money: {_money} ");
        _moneyText.text = _money.ToString();

    }
    public override void Configure(object[] values)
    {
        _speedCost = GetParameter<int>(values[0]);
        _quantityCost = GetParameter<int>(values[1]);
        _damageCost = GetParameter<int>(values[2]);
        _healCost = GetParameter<int>(values[3]);
    }

    public override Action<object> SubscribeMethodToExternalAction(Enum method)
    {
        return _externalActionReceiver[method];
    }
    public override void SubscribeExternalActionsSenders(Action<object> receiver)
    {
        _attributeActionReceiver = receiver;
        _attributeActionSender += _attributeActionReceiver;
    }

    public override void UnsubscribeExternalActionsSender()
    {
        _attributeActionSender -= _attributeActionReceiver;
    }
    
}