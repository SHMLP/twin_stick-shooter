using System.Collections.Generic;
using UnityEngine;

public class UIControllerLocator : MonoBehaviour
{
    private IController[] _controllersList;
    private Dictionary<EnumScreenType, IController> _uiControllersContainer = new Dictionary<EnumScreenType, IController>(); 
     
    public void CreateControllersList()
    {
        _controllersList = GetComponentsInChildren<IController>(true);
        foreach (IController item in _controllersList)
        {
            _uiControllersContainer.Add(item.GetUIType(),item);
        }

    }

    public Dictionary<EnumScreenType, IController> GetControllersList()
    {
        return _uiControllersContainer;
    }
}