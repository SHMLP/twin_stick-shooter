using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBaseView<T> : MonoBehaviour, IView
{
    [SerializeField] private ScreenNavigationButtons[] _navigationButtonsContainer;
    
    [SerializeField] private ButtonsFunctions<T>[] _functionsButtonsContainer;
    private Dictionary<T, object[]> _parametersContainer = new Dictionary<T, object[]>();
    private Dictionary<T, object[]> _parametersWithoutButtonsContainer = new Dictionary<T, object[]>();
    
    private object[] _parameterList;

    protected object[] ParameterList
    {
        get => _parameterList;
        set => _parameterList = value;
    }

    public virtual void Init()
    {
        
    }
    
    protected Dictionary<T, object[]> ParametersContainer
    {
        get => _parametersContainer;
        set => _parametersContainer = value;
    } 
    protected Dictionary<T, object[]> ParametersWithoutButtonsContainer
    {
        get => _parametersWithoutButtonsContainer;
        set => _parametersWithoutButtonsContainer = value;
    }
    
    
    public ButtonsFunctions<U>[]  GetFunctionButtonsContainer<U>() 
    {
        return _functionsButtonsContainer as ButtonsFunctions<U>[];
    }
    public Dictionary<U, object[]> GetParametersContainer<U>()
    {
        return ParametersContainer as Dictionary<U, object[]> ;
    }
    
    public Dictionary<U, object[]> GetParametersWithoutButtonsContainer<U>()
    {
        return ParametersWithoutButtonsContainer as Dictionary<U, object[]>;
    } 
    public ScreenNavigationButtons[] GetNavigationButtons()
    {
        return _navigationButtonsContainer;
    }
}