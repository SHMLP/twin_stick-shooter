using System.Collections.Generic;

public abstract class AbstractBaseModel<T> : IModel
{
    private Dictionary<T, object[]> _valuesContainer = new Dictionary<T, object[]>();
    private List<object> _modelValuesList;
    protected List<object> ModelValuesList
    {
        get => _modelValuesList;
        set => _modelValuesList = value;
    }
    public Dictionary<T, object[]> ValuesContainer
    {
        get => _valuesContainer;
        set => _valuesContainer = value;
    }

    public virtual void Init()
    {
        
    }

    public Dictionary<U, object[]> GetValuesContainer<U>()
    {
        return _valuesContainer as Dictionary<U, object[]> ;
    }
}

