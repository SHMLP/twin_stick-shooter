﻿using System;

public interface ILife
{
    void SubscribeDieAction(Action receiver);
    public void Configure(int maxlife);
    public void ReduceLife(int amount);
    public void UnsubscribeActions();
    
}