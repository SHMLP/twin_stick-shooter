﻿using UnityEngine;

public class PlayerMover : IMovable
{
    private Transform _playerTransform;
    private  float _velocity;

    private Vector2 _moveDirection;
    
    public PlayerMover(Transform playerTransform, float velocity)
    {
        _playerTransform = playerTransform;
        _velocity = velocity;
    }

    public void DoMove()
    {
        _moveDirection.x = Input.GetAxisRaw("Horizontal");
        _moveDirection.y = Input.GetAxisRaw("Vertical");
        
        _playerTransform.Translate(_moveDirection*_velocity*Time.deltaTime);
    }

    public void SetValue(object[] values)
    {
        
    }
}