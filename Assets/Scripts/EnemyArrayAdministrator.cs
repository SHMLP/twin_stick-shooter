﻿using System;
using UnityEngine;

public class EnemyArrayAdministrator
{
    private event Action<Enemy> _startEnemyActionSender;
    private event Action<Enemy> _startEnemyActionReceiver;

    public void SubscribeStartEnemyAction(Action<Enemy> receiver)
    {
        _startEnemyActionReceiver = receiver;
        _startEnemyActionSender += _startEnemyActionReceiver;
    }
    public void UnsubscribeStartEnemyAction()
    {
        _startEnemyActionSender -= _startEnemyActionReceiver;
    }
    
    public void RemoveEnemiesFromArray(Enemy[] enemiesToSpawn ,ref int lastRemoveIndex,ref int maxNumberOfEnemies)
    {
        int currentEnemiesSpawn = 0;
        int initialIndex = lastRemoveIndex;
        int maxIndex = initialIndex + maxNumberOfEnemies;
        for (int i = initialIndex; i < maxIndex; i++)
        {
            if (i>enemiesToSpawn.Length-1)
            {
                break;
            }
            if (enemiesToSpawn[i]!=null)
            {
                //Debug.Log(enemiesToSpawn[i]);
                _startEnemyActionSender?.Invoke(enemiesToSpawn[i]);
                //enemiesToSpawn = RemoveEnemyOfArray(enemiesToSpawn, i);
                lastRemoveIndex = UpdateIndex(enemiesToSpawn.Length,ref i);
                currentEnemiesSpawn++;
            }
            else
            {
                break;
            }
            int missingAmount = maxNumberOfEnemies - currentEnemiesSpawn;
            if (i==enemiesToSpawn.Length-1 && missingAmount>0)
            {
                maxIndex = missingAmount;
                i = -1;
                initialIndex = 0;
            }
        }
    }

    public int UpdateIndex(int arrayLength,ref int index)
    {
        int newIndex = 0;
        if (index<arrayLength-1)
        {
            newIndex = index;
            newIndex++;
            return newIndex;
        }
        return newIndex;
    }
    private Enemy[] RemoveEnemyOfArray(Enemy[] enemies, int index)
    {
        enemies[index] = null;
        return enemies;
    }
    public Enemy[] AddEnemyOfArray(Enemy[] enemies, Enemy enemy,ref int lastAddIndex)
    {
        enemies[lastAddIndex] = enemy;
        return enemies;
    }
}