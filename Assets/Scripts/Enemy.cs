﻿using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private EnemyTeleporter _enemyTeleporter;
    private EnemyLife _enemyLife;
    private EnemyActivator _enemyActivator;
    private EnemyMovementController _enemyMovementController;

    public event Action<Enemy> _enemyDeadActionSender;
    public event Action<Enemy> _enemyDeadActionReceiver;
    
    public void InitComponents(Transform playerTransform,float _spawnInternalRadius,float _spawnExternalRadius,float _velocity)
    {
        _enemyLife = GetComponent<EnemyLife>();
        _enemyLife.Configure(100);
        _enemyLife.SubscribeDieAction(OnDead);
        
        _enemyMovementController = GetComponent<EnemyMovementController>();
        _enemyMovementController.SetMovement(new EnemyMover(transform,playerTransform,_velocity));
        
        _enemyActivator = new EnemyActivator(gameObject);
        
        _enemyTeleporter = new EnemyTeleporter(transform, _spawnInternalRadius,_spawnExternalRadius);
    }

    public void SubscribeEnemyDeadAction(Action<Enemy> receiver)
    {
        _enemyDeadActionReceiver = receiver;
        _enemyDeadActionSender += _enemyDeadActionReceiver;
    }
    private void UnsubscribeEnemyDeadAction()
    {
        _enemyDeadActionSender -= _enemyDeadActionReceiver;
    }
    public void StartEnemy()
    {
        EnemyTeleport();
        _enemyActivator.Activate();
        _enemyMovementController.StartMovement();
    }

    public void StopOwnMovement()
    {
        _enemyMovementController.StopMovement();
    }
    private void OnDead()
    {
        Disappear();
        _enemyDeadActionSender?.Invoke(this);
    }

    public void Disappear()
    {
        _enemyActivator.Deactivate();

    }
    private void EnemyTeleport()
    {
        _enemyTeleporter.Teleport();
    }
    
    public void UnsubscribeAllActions()
    {
        _enemyLife.UnsubscribeActions();
        UnsubscribeEnemyDeadAction();
    }
    
}