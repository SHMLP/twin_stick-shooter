﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RoundStatesFunctionStorekeeper
{
    
    private Dictionary<RoundStateType, Action> _functionsContainer = new();


    public void AddFunction(RoundStateType roundStateType, Action function)
    {
        _functionsContainer.Add(roundStateType,function);
    }

    public void InvokeFunction(object attribute)
    {
        RoundStateType roundStateType = (RoundStateType)attribute;
        _functionsContainer[roundStateType]?.Invoke();
    }

}