﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyTeleporter
{
    private Transform _enemyTransform;
    private float _spawnInternalRadius;
    private float _spawnExternalRadius;

    public EnemyTeleporter(Transform enemyTransform, float spawnInternalRadius, float spawnExternalRadius)
    {
        _enemyTransform = enemyTransform;
        _spawnInternalRadius = spawnInternalRadius;
        _spawnExternalRadius = spawnExternalRadius;
    }
    
    public void Teleport()
    {
        float radius = Random.Range(_spawnInternalRadius, _spawnExternalRadius);
        float radiusSign = Mathf.Sign(Random.Range(-1, 1));
        float xPosition = Random.Range(0, radiusSign * radius);
        float yPosition = Mathf.Sqrt((radius*radius)-(xPosition*xPosition));
        yPosition = Mathf.Sign(Random.Range(-1, 1))*yPosition;
        _enemyTransform.position = new Vector2(xPosition,yPosition);
    }
    
}